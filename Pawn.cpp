#include "Pawn.h"

bool Pawn::isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const
{
	bool returnValue = true;

	if (dest.getX() == src.getX() && !b[dest.getY()][dest.getX()])
	{
		if ((dest.getY() - src.getY() == 1 || (dest.getY() - src.getY() == 2 && src.getY() == 1))
			&& b[src.getY()][src.getX()]->getColor() == 'b') {
			return true;
		}
		else if ((src.getY() - dest.getY() == 1 || (src.getY() - dest.getY() == 2 && src.getY() == 6))
			&& b[src.getY()][src.getX()]->getColor() == 'w') 
{
			return true;
		}
	}
	else if(b[dest.getY()][dest.getX()]!= nullptr)// checking if the pawn is trying to eat
	{
		//white
		if (abs(dest.getX() - src.getX()) == 1 && dest.getY() - src.getY() == -1 &&
			b[src.getY()][src.getX()]->getColor() == 'w')
		{
			return true;
		}
		//black
		else if (abs(dest.getX() - src.getX()) == 1 && dest.getY() - src.getY() == 1 &&
			b[src.getY()][src.getX()]->getColor() == 'b')
		{
			return true;
		}
	}
	return false;
	
}