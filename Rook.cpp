#include "Rook.h"



Rook::Rook(PieceName type, char color):Piece(type,color)
{
	this->_color = color;
	this->_type = type;
}


Rook::~Rook()
{

}


bool Rook::isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const
{
	Cube start(dest);
	Cube end(src);

	if (dest.getX() > src.getX() || dest.getY() > src.getY()) {
		start = src;
		end = dest;
	}

	if (src.getX() == dest.getX())
	{
		for (int i = 1; start.getY() + i < end.getY() ; i++) {
			if (b[start.getY() + i][start.getX()] &&
				b[start.getY()+ i][start.getX()] != b[src.getY()][src.getX()]) {
				return false;
			}
		}
	}
	else if (src.getY() == dest.getY())
	{
		for (int i = 1; (start.getX() + i) < end.getX() ; i++)
		{
			if (b[start.getY()][start.getX() +i ] && 
				b[start.getY()][start.getX() + i] != b[src.getY()][src.getX()]) {
				return false;
			}
		}
	}
	else
	{
		return false;
	}
	return true;//if arrives to here ,everything is valid
}

