#include "Queen.h"



Queen::Queen(PieceName type, char color):Piece(type,color),Rook(type,color),Bishop(type,color)
{
	this->_type = type;
	this->_color = color;
}



Queen::~Queen()
{
}

bool Queen::isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const
{
	if (Rook::isValidMove(src, dest, b) || Bishop::isValidMove(src, dest, b))
	{
		return true;
	}
	else
	{
		return false;
	}
}