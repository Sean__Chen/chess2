#include "Piece.h"

Piece& Piece::operator=(Piece& other)
{
	this->color = other.getColor();
	this->type = other.getType();
	return *this;
}