#include "Borad.h"



Borad::Borad(string& data, char player):_WhiteKing(Cube(0,0)), _BlackKing(Cube(0, 0)) {
	this->player = player;
	int index = 0;
	for (int i = 0; i < LENGTH; ++i) 
	{
		for (int j = 0; j < WIDTH; ++j) 
		{
			switch (data[index])
			{
				case 'r':
					this->board[i][j] = new Rook(RookName, 'b');
					this->tempBoard[i][j] = new Rook(RookName, 'b');
					break;
				case 'R':
					this->board[i][j] = new Rook(RookName, 'w');
					this->tempBoard[i][j] = new Rook(RookName, 'w');
					break;
				case 'b':
					this->board[i][j] = new Bishop(BishopName, 'b');
					this->tempBoard[i][j] = new Bishop(BishopName, 'b');
					break;
				case 'B':
					this->board[i][j] = new Bishop(BishopName, 'w');
					this->tempBoard[i][j] = new Bishop(BishopName, 'w');
					break;
				case 'q':
					this->board[i][j] = new Queen(QueenName, 'b');
					this->tempBoard[i][j] = new Queen(QueenName, 'b');
					break;
				case 'Q':
					this->board[i][j] = new Queen(QueenName, 'w');
					this->tempBoard[i][j] = new Queen(QueenName, 'w');
					break;
				case 'n':
					this->board[i][j] = new Knight(KnightName, 'b');
					this->tempBoard[i][j] = new Knight(QueenName, 'b');
					break;
				case 'N': 
					this->board[i][j] = new Knight(KnightName, 'w');
					this->tempBoard[i][j] = new Knight(KnightName, 'w');
					break;
				case 'p':
					this->board[i][j] = new Pawn(PawnName, 'b');
					this->tempBoard[i][j] = new Pawn(PawnName, 'b');
					break;
				case 'P':
					this->board[i][j] = new Pawn(PawnName, 'w');
					this->tempBoard[i][j] = new Pawn(PawnName, 'w');
					break;
				case 'k':
					this->board[i][j] = new King(KingName, 'b');
					this->tempBoard[i][j] = new King(KingName, 'b');
					this->_BlackKing = Cube(j, i);
					break;
				case 'K':
					this->board[i][j] = new King(KingName, 'w');
					this->tempBoard[i][j] = new King(KingName, 'w');
					this->_WhiteKing = Cube(j,i);
					break;
				case '#':
					this->board[i][j] = nullptr;
					this->tempBoard[i][j] = nullptr;
					break;
				default:
					break;
			}
			index++;
		}
	}
}


Borad::~Borad()
{
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			if (this->board[i][j] != nullptr)
			{
				this->board[i][j]->~Piece(); //D'tor of the object
			}
			if (this->tempBoard[i][j] != nullptr)
			{
				this->tempBoard[i][j]->~Piece();
			}
		}
	}
}

char Borad::isValidMove(string& data)
{
	Cube src = getCube(data.substr(0, 2));
	Cube dest = getCube(data.substr(2, 2));
	Cube* temp[2] = { &this->_BlackKing, &this->_WhiteKing };
	// checking that there is a piece in the src cube and the piece that the plays
	//is moving is his piece
	if (this->board[src.getY()][src.getX()] == nullptr || 
		this->board[src.getY()][src.getX()]->getColor() != this->player)
	{
		return '2';
	}
	else if (this->board[dest.getY()][dest.getX()] != nullptr &&
		this->player == this->board[dest.getY()][dest.getX()]->getColor())
	{
		return '3';
	}
	else if (src.getX() >= LENGTH || src.getX() < 0 || src.getY() >= WIDTH || src.getY() < 0 ||
		dest.getX() >= LENGTH || dest.getX() < 0 || dest.getY() >= WIDTH || dest.getY() < 0)
	{
		return '5';
	}
	else if (!this->board[src.getY()][src.getX()]->isValidMove(src, dest, this->board))
	{
		return '6';
	}
	else if (src == dest) {
		return '7';
	}
	else
	{
		this->updateBoard(src, dest);
		updateKings(dest);
		if (this->checkChess(_WhiteKing, 'b')) { // cheking the white king
			if (this->player == 'b')//black has made a check on the white 
			{
				setPlayer(this->player);
				return '1';
			}
			else//white has chess and doesn't cancel it
			{
				updateKings(*temp[1]);
				restoreBoard();
				return '4';
			}
		}
		else if (this->checkChess(_BlackKing, 'w')) { // checking the black king
			if (this->player == 'w')//white has made a check on the black
			{
				setPlayer(this->player);
				return '1';
			}
			else //black has chess and doesn't cancel it
			{
				updateKings(*temp[0]);
				restoreBoard();
				return '4';
			}
		}
		setPlayer(this->player);
		return '0';
	}
}

void Borad::updateKings(Cube& dest)
{
	if (this->board[dest.getY()][dest.getX()]->getType() == KingName) {
		if (this->board[dest.getY()][dest.getX()]->getColor() == 'w') {
			this->_WhiteKing = dest;
		}
		else {
			this->_BlackKing = dest;
		}
	}
} 



Cube Borad::getCube(string data) const
{
	int y = abs(data[1] - 56);//casting
	int x = abs(data[0] - 97);//casting
	Cube c = Cube(x, y);
	return c;
}


/*
	-the function being called if and only if 
     the move is valid
	-The function updates the board
	input: Cube src and Cube dest
	output: none
*/
void Borad::updateBoard(Cube& src, Cube& dest)
{
	//saving the board
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			tempBoard[i][j] = board[i][j];
		}
	}
	this->board[dest.getY()][dest.getX()] = this->board[src.getY()][src.getX()];//putting the src piece in the dest
	this->board[src.getY()][src.getX()] = nullptr;//make the src empty
}

/*
	printing the board
*/
void Borad::printBoard()
{
	for (int i = 0; i < WIDTH; i++)
	{
		for (int j = 0; j < LENGTH; j++)
		{
			if (this->board[i][j] == nullptr)
			{
				cout << "# ";
			}
			else
			{
				switch (this->board[i][j]->getType())
				{
				case RookName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "r ";
					}
					else
					{
						cout << "R ";
					}
					break;
				case BishopName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "b ";
					}
					else
					{
						cout << "B ";
					}
					break;
				case QueenName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "q ";
					}
					else
					{
						cout << "Q ";
					}
					break;
				case KingName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "k ";
					}
					else
					{
						cout << "K ";
					}
					break;
				case KnightName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "n ";
					}
					else
					{
						cout << "N ";
					}
					break;
				case PawnName:
					if (this->board[i][j]->getColor() == 'b')
					{
						cout << "p ";
					}
					else
					{
						cout << "P ";
					}
					break;
				default:
					cout << "invalid note!";
					break;
				}
			}
		}
		cout << endl;
	}
}


/*
	Arrives to the function if the move is valid
	Function switches the player
	input: the current color
	output: none
*/
void Borad::setPlayer(char color)
{
	if (color == 'b')
	{
		this->player = 'w';
	}
	else
	{
		this->player = 'b';
	}
}


bool Borad::checkChess(Cube& kingLocation, char color)
{
	Cube temp(0, 0);
	for (int i = 0; i < LENGTH; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			if (this->board[i][j] != nullptr && this->board[i][j]->getColor() == color)
			{
				//current piece
				temp.setX(j);temp.setY(i);
				if (this->board[i][j]->isValidMove(temp, kingLocation, this->board))
				{
					return true;
				}
			}
		}
	}
	return false;
}
 
void Borad::restoreBoard() {
	for (int i = 0; i < LENGTH; i++) {
		for (int j = 0; j < WIDTH; j++)
		{
			this->board[i][j] = this->tempBoard[i][j];
		}
	}
}

