#include "Bishop.h"



Bishop::Bishop(PieceName type, char color):Piece(type,color)
{
	this->_color = color;
	this->_type = type;
}


Bishop::~Bishop()
{
}

bool Bishop::isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const
{
	Cube start(dest);
	Cube end(src);
	int adder = 1;

	if (abs(dest.getX() - src.getX()) == abs(dest.getY() - src.getY())) {
		if (src.getX() > dest.getX() && dest.getY() > src.getY()) {
			for (int i = 1; (src.getY() + i) != dest.getY() ; i++) {
				if (b[src.getY() + i][src.getX() - adder] &&
					b[src.getY() + i][src.getX() - adder] != b[src.getY()][src.getX()]) {
					return false;
				}
				adder++;
			}
		}
		else if (dest > src) {
			for (int i = 1; (src.getX() + i) != dest.getX(); i++) {
				if (b[src.getY() + i][src.getX() + i] &&
					b[src.getY() + i][src.getX() + i] != b[src.getY()][src.getX()]) {
					return false;
				}
			}
		}
		else if (src > dest) {
			for (int i = 1; (start.getX() + i) != end.getX(); i++) {
				if (b[start.getY() + i][start.getX() + i] &&
					b[start.getY() + i][start.getX() + i] != b[src.getY()][src.getX()]) {
					return false;
				}
			}
		}
		else {
			for (int i = 1; (src.getX() + i) != dest.getX(); i++) {
				if (b[src.getY() - adder][src.getX() + i] &&
					b[src.getY() - adder][src.getX() + i] != b[src.getY()][src.getX()]) {
					return false;
				}
				adder++;
			}
		}
	}
	else {
		return false;
	}
	return true;
}
