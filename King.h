#pragma once

#include "Piece.h"
#include "Cube.h"

#define STEP 1

class King : public Piece
{
public:

	King(PieceName type, char color) : Piece(type, color) {
		this->_type = type;
		this->_color = color;
	}

	~King() {}

	virtual bool isValidMove(Cube& src, Cube& dest, Piece* b[][LENGTH]) const {
		if (abs(src.getX() - dest.getX()) == STEP && abs(src.getY()- dest.getY()) == STEP ||
			abs(src.getX() - dest.getX()) == 0 && abs(src.getY() - dest.getY()) == STEP ||
			abs(src.getX() - dest.getX()) == STEP && abs(src.getY() - dest.getY()) == 0)
		{
			return true;
		}
		return false;
	}

	virtual PieceName getType() const { return this->_type; }
	virtual char getColor() const { return this->_color; }

private:
	PieceName _type;
	char _color;
};

