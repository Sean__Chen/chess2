#pragma once
#include "Piece.h"
class Knight: public Piece
{
public:
	Knight(PieceName type, char color) :Piece(type, color) {
		this->_type = type;
		this->_color = color;
	}
	virtual bool isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const;
	virtual PieceName getType() const { return this->_type; }
	virtual char getColor() const { return this->_color; }

private:
	PieceName _type;
	char _color;
};

