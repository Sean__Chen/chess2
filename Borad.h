#pragma once
#include <iostream>
#include <stack>
#include "Piece.h"
#include "Cube.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "Knight.h"
#include "Pawn.h"
#include "King.h"

#define LENGTH 8
#define WIDTH 8

using std::cout;
using std::endl;


class Borad
{
public:
	Borad(string& data, char player);
	~Borad();
	char isValidMove(string& data);
	void printBoard();
	void updateBoard(Cube& src, Cube& dest);
	
	
	
private:
	Cube _WhiteKing;
	Cube _BlackKing;
	Piece* board[LENGTH][WIDTH];
	Piece* tempBoard[LENGTH][WIDTH];
	
	char player; // b = black, w = white
	Cube getCube(string data) const;

	void updateKings(Cube& dest);
	void setPlayer(char color);
	void restoreBoard();
	bool checkChess(Cube& kingLocation, char color);
};

